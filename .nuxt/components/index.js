export { default as Logo } from '../../components/Logo.vue'
export { default as NavBarMobile } from '../../components/NavBarMobile.vue'
export { default as VuetifyLogo } from '../../components/VuetifyLogo.vue'

export const LazyLogo = import('../../components/Logo.vue' /* webpackChunkName: "components/Logo" */).then(c => c.default || c)
export const LazyNavBarMobile = import('../../components/NavBarMobile.vue' /* webpackChunkName: "components/NavBarMobile" */).then(c => c.default || c)
export const LazyVuetifyLogo = import('../../components/VuetifyLogo.vue' /* webpackChunkName: "components/VuetifyLogo" */).then(c => c.default || c)
