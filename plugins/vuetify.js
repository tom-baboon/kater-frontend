import Vue from 'vue'
import Vuetify from 'vuetify'

import 'vuetify/dist/vuetify.min.css'
Vue.use(Vuetify)

export default ctx => {
  const vuetify = new Vuetify({
    customVariables: ['~/assets/variables.scss'],
    treeShake: true,
    ssr: true,
    defaultAssets:  {icons: 'fa'},
    theme: {
      light: true,  //you don't actually need this line as it's for default
      themes: {
          light: {
              primary: '#A3AC88',
              secondary: '#4A373E',
          }
      }
    }
  })
  ctx.app.vuetify = vuetify
  ctx.$vuetify = vuetify.framework
}
